 var map;
function initialize() {
	var myLatlng = new google.maps.LatLng(56.859619, 35.879264)
	var mapOptions = {
    zoom: 16,
    scrollwheel: false,
    center: myLatlng
};
map = new google.maps.Map(document.getElementById('maps_block'),
  mapOptions);


google.maps.event.addDomListener(window, 'load', initialize);

var marker_content ='<div class="block_mark_maps">'+
						'<div class="block_mark_maps-content">'+
							'<div class="mark_maps-logo"></div>'+
							'<div class="mark_maps-text">'+
								'<span>Тверь, Беляковский пер.,  д.36</span>'+
								'<span>Тел.: 8 (4822) 42-31-32</span>'+
						'</div>'+
					'</div>';
var infowindow = new google.maps.InfoWindow({
    position: myLatlng,
    maxWidth: 100,
    content: marker_content,
    disableAutoPan: true
});
infowindow.open(map);
}

google.maps.event.addDomListener(window, 'load', initialize);



 $(document).ready(function(){
 	$('#slider').flexslider({
	    animation: "slide",
	    prevText: "", 
		nextText: "", 
		controlNav: false
	});
 	$(".arrival_content-name").on("click", function(){
 		var cart_goods = $(this).parents("li");
 		if($(cart_goods).hasClass("active")){
 			$(cart_goods).removeClass("active");
 			$(cart_goods).find(".arrival_content-description").slideUp("fast");
 		} else {
 			$(cart_goods).addClass("active");
 			$(cart_goods).find(".arrival_content-description").slideDown("fast");
 		}
 	});


 	// acrdion
	var accordion = function(selector){
		$(selector).parents("li").find("ul").css({"display" : "none"})
		$(selector).click(function(){
		  	var elem_sitebar= $(this).parents("li");
		    if($(elem_sitebar).find("ul > li ul")){
			    $(elem_sitebar).find("ul > li ul").css({"display" : "none"});
			    $(elem_sitebar).find("ul > li").removeClass("active");
		    }

		    if($(elem_sitebar).hasClass("active"))
		    {
		    	$(elem_sitebar).parents("ul").find("ul").slideUp("fast");
		    	$(elem_sitebar).removeClass("active");
		    }else{
		  		$(elem_sitebar).parents("ul").find("li").removeClass("active");
	  			$(elem_sitebar).parents("ul").find("ul").slideUp("fast");
		        $(elem_sitebar).addClass("active");
		        $(elem_sitebar).find("ul").slideDown("fast");

		    }
	    });
	};

	accordion($(".sitebar_cat-name"));

	// spinner
	var spinner = $(".basket_block-col input[type='text']");
	$(spinner).val(1)
	var min_spiner = 0;
	var max_spiner = 100;




	$( spinner ).spinner({
		spin: function( event, ui ) {
        if ( ui.value > max_spiner ) {
          $( this ).spinner( "value", min_spiner );
          return false;
        } else if ( ui.value < min_spiner ) {
          $( this ).spinner( "value", max_spiner );
          return false;
        }
      }
	});
	$(spinner).focus(function(){
		$(this).keyup(function(){
			if(parseInt($(this).val()) >= max_spiner){
    			$(this).val(max_spiner)
    		}else if($(this).val() <= min_spiner){
				$(this).val(min_spiner)
			}
		})
	})
	// end

	// destroy goods
	$(".basket_block-destroy").click(function(){
		$(this).parents("li").remove();
	})

	// scrollbar	
	$(".basket_goods-content").mCustomScrollbar();

	// colorbox
	$('.image_goods').colorbox({
		fixed: true,
		maxWidth: "95%",
		maxHeight: "95%"
	});
 });
